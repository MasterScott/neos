#pragma once

// Classes for the various handles returned in eos_sdk.cpp
// (we only need classes for the kinds that hold some kind of state, eg. HAuth)
// Note that these are likely nowhere near the actual EOS HAuth/HPlatform/etc, as definitions for those aren't included in the SDK
// It shouldn't matter though since these are opaque to any SDK users anyway
// (maybe Epic titles could make use of internal functions, they seem to have EOS built-in though)

class HPlatform;

class HAuth
{
	HPlatform* m_Platform = nullptr;

	std::mutex m_LoginCallbacksMutex;
	std::vector<std::function<void()>> m_LoginCallbacks;

	std::mutex m_LoginStatusMutex;
	EOS_ELoginStatus m_PrevStatus = EOS_ELoginStatus::EOS_LS_NotLoggedIn;
	EOS_ELoginStatus m_LoginStatus = EOS_ELoginStatus::EOS_LS_NotLoggedIn;

public:
	HAuth(HPlatform* Platform) : m_Platform(Platform) {}

	size_t LoginCallbackAdd(std::function<void()> Func);
	void LoginCallbackRemove(size_t ID);

	EOS_ELoginStatus GetPrevLoginStatus() { return m_PrevStatus; }
	EOS_ELoginStatus GetLoginStatus() { return m_LoginStatus; }
	void ChangeLoginStatus(EOS_ELoginStatus NewStatus);
};

class HEcom
{
	HPlatform* m_Platform = nullptr;

	std::mutex m_EntitlementsMutex;
	std::vector<EOS_Ecom_Entitlement*> m_Entitlements;

	EOS_HEcom m_ProxyHandle = nullptr;

public:
	HEcom(HPlatform* Platform) : m_Platform(Platform) {}

	size_t GetEntitlementCount() { return m_Entitlements.size(); }
	EOS_Ecom_Entitlement* CacheEntitlement(size_t instanceId);

	EOS_Ecom_Entitlement* GetEntitlementByEntitlementId(const std::string& entitlementId);
	EOS_Ecom_Entitlement* GetEntitlementByInstanceId(size_t instanceId);
	EOS_Ecom_Entitlement* GetEntitlementByIndex(size_t index);

	void SetProxyHandle(EOS_HEcom handle) { m_ProxyHandle = handle; }
	EOS_HEcom GetProxyHandle() { return m_ProxyHandle; }
};

class HPlatform
{
	HAuth m_HAuth;
	HAuth m_HConnect; // Uses same code as HAuth :)
	HEcom m_HEcom;

	EOS_HPlatform m_ProxyHandle = nullptr;

public:
	HPlatform() : m_HAuth(this), m_HConnect(this), m_HEcom(this) {}

	// Pretty much a copy of EOS_Platform_Options
	// But using std::string instead of const char*

	int32_t m_ApiVersion = 0;
	void* m_Reserved = nullptr;
	std::string m_ProductId;
	std::string m_SandboxId;

	struct
	{
		std::string ClientId;
		std::string ClientSecret;
	} m_ClientCredentials;

	bool m_bIsServer = false;

	//--1.2.0
	std::string m_EncryptionKey;
	std::string m_OverrideCountryCode;
	std::string m_OverrideLocaleCode;
	std::string m_DeploymentId;
	uint64_t m_Flags;
	//--1.2.0

	HAuth* GetHAuth() { return &m_HAuth; }
	HAuth* GetHConnect() { return &m_HConnect; }
	HEcom* GetHEcom() { return &m_HEcom; }

	void SetProxyHandle(EOS_HPlatform handle) { m_ProxyHandle = handle; }
	EOS_HPlatform GetProxyHandle() { return m_ProxyHandle; }
};

#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Returns a string representation of an EOS_EResult. 
 * The return value is never null.
 * The return value must not be freed.
 *
 * Example: EOS_EResult_ToString(EOS_Success) returns "EOS_Success"
 */
EOS_DECLARE_FUNC(const char*) EOS_EResult_ToString(EOS_EResult Result)
{
	return EResultToString(Result);
}

/**
 * Check whether or not the given account unique id is considered valid
 *
 * @param AccountId The account id to check for validity
 * @return EOS_TRUE if the EOS_ProductUserId is valid, otherwise EOS_FALSE
 */
EOS_DECLARE_FUNC(EOS_Bool) EOS_ProductUserId_IsValid(EOS_ProductUserId AccountId)
{
	log(LL::Debug, "EOS_ProductUserId_IsValid (%p)", AccountId);

	PROXY_FUNC(EOS_ProductUserId_IsValid);
	if (proxied)
	{
		auto res = proxied(AccountId);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	return AccountId == UserProductId;
}

/**
 * Retrieve a string-ified account ID from an EOS_ProductUserId. This is useful for replication of Product User IDs in multiplayer games.
 *
 * @param AccountId The account ID for which to retrieve the string-ified version.
 * @param OutBuffer The buffer into which the character data should be written
 * @param InOutBufferLength The size of the OutBuffer in characters.
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates whether the account ID string was copied into the OutBuffer.
 *         EOS_Success - The OutBuffer was filled, and InOutBufferLength contains the number of characters copied into OutBuffer excluding the null terminator.
 *         EOS_InvalidParameters - Either OutBuffer or InOutBufferLength were passed as NULL parameters.
 *         EOS_InvalidUser - The AccountId is invalid and cannot be string-ified
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the account ID string. InOutBufferLength contains the required minimum length to perform the operation successfully.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_ProductUserId_ToString(EOS_ProductUserId AccountId, char* OutBuffer, int32_t* InOutBufferLength)
{
	log(LL::Debug, "EOS_ProductUserId_ToString (%p, %p, %p(%d))", AccountId, OutBuffer, InOutBufferLength, *InOutBufferLength);

	PROXY_FUNC(EOS_ProductUserId_ToString);
	if (proxied)
	{
		auto res = proxied(AccountId, OutBuffer, InOutBufferLength);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	// EpicAccountId function works how we need it, piggyback off that
	return EOS_EpicAccountId_ToString(reinterpret_cast<EOS_EpicAccountId>(AccountId), OutBuffer, InOutBufferLength);
}

/**
 * Retrieve an EOS_EpicAccountId from a raw account ID string. The input string must be null-terminated.
 *
 * @param AccountIdString The string-ified account ID for which to retrieve the EOS_ProductUserId
 * @return The EOS_ProductUserId that corresponds to the AccountIdString
 */
EOS_DECLARE_FUNC(EOS_ProductUserId) EOS_ProductUserId_FromString(const char* AccountIdString)
{
	log(LL::Debug, "EOS_ProductUserId_FromString (%p(%s))", AccountIdString, AccountIdString);

	PROXY_FUNC(EOS_ProductUserId_FromString);
	if (proxied)
	{
		auto res = proxied(AccountIdString);
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	// EpicAccountId function works how we need it, piggyback off that
	return reinterpret_cast<EOS_ProductUserId>(EOS_EpicAccountId_FromString(AccountIdString));
}

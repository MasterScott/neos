#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Release the memory associated with an EOS_Presence_Info structure and its sub-objects. This must be called on data retrieved from EOS_Presence_CopyPresence.
 * This can be safely called on a NULL presence info object.
 *
 * @param PresenceInfo The presence info structure to be release
 */
EOS_DECLARE_FUNC(void) EOS_Presence_Info_Release(EOS_Presence_Info* PresenceInfo)
{
	log(LL::Debug, "EOS_Presence_Info_Release");

	PROXY_FUNC(EOS_Presence_Info_Release);
	if (proxied)
	{
		proxied(PresenceInfo);
		return;
	}

	return;
}

/**
 * Release the memory associated with an EOS_HPresenceModification handle. This must be called on Handles retrieved from EOS_Presence_CreatePresenceModification.
 * This can be safely called on a NULL presence modification handle. This also may be safely called while a call to SetPresence is still pending.
 *
 * @param PresenceModificationHandle The presence modification handle to release
 *
 * @see EOS_Presence_CreatePresenceModification
 */
EOS_DECLARE_FUNC(void) EOS_PresenceModification_Release(EOS_HPresenceModification PresenceModificationHandle)
{
	log(LL::Debug, "EOS_PresenceModification_Release");

	PROXY_FUNC(EOS_PresenceModification_Release);
	if (proxied)
	{
		proxied(PresenceModificationHandle);
		return;
	}

	return;
}

/**
 * The Presence methods allow you to query, read other player's presence information, as well as to modify your own.
 *
 * QueryPresence must be called once per login, per remote user, before data will be available. It is currently only possible to query presence for
 * users that are on your friends list, all other queries will return no results.
 */

 /**
  * Query a user's presence. This must complete successfully before CopyPresence will have valid results. If HasPresence returns true for a remote
  * user, this does not need to be called.
  *
  * @param Options Object containing properties related to who is querying presence and for what user
  * @param ClientData Optional pointer to help track this request, that is returned in the completion callback
  * @param CompletionDelegate Pointer to a function that handles receiving the completion information
  */
EOS_DECLARE_FUNC(void) EOS_Presence_QueryPresence(EOS_HPresence Handle, const EOS_Presence_QueryPresenceOptions* Options, void* ClientData, const EOS_Presence_OnQueryPresenceCompleteCallback CompletionDelegate)
{
	log(LL::Debug, "EOS_Presence_QueryPresence (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_PRESENCE_QUERYPRESENCE_API_LATEST);

	PROXY_FUNC(EOS_Presence_QueryPresence);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_Presence_QueryPresenceOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_Presence_QueryPresenceCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;
			cbi.TargetUserId = Options ? options.TargetUserId : nullptr;
			if (Options)
				cbi.ResultCode = options.TargetUserId == UserAccountId ? EOS_EResult::EOS_Success : EOS_EResult::EOS_NotFound;

			CompletionDelegate(&cbi);
		});
}

/**
 * Check if we already have presence for a user
 *
 * @param Options Object containing properties related to who is requesting presence and for what user
 * @return EOS_TRUE if we have presence for the requested user, or EOS_FALSE if the request was invalid or we do not have cached data
 */
EOS_DECLARE_FUNC(EOS_Bool) EOS_Presence_HasPresence(EOS_HPresence Handle, const EOS_Presence_HasPresenceOptions* Options)
{
	log(LL::Debug, "EOS_Presence_HasPresence");

	EOS_CHECK_VERSION(EOS_PRESENCE_HASPRESENCE_API_LATEST);

	PROXY_FUNC(EOS_Presence_HasPresence);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	if (!EOS_IsConfigured())
		return false;

	if (!Options)
		return false;

	return true;
}

/**
 * Get a user's cached presence object. If successful, this data must be released by calling EOS_Presence_Info_Release
 *
 * @param Options Object containing properties related to who is requesting presence and for what user
 * @param OutPresence A pointer to a pointer of Presence Info. If the returned result is success, this will be set to data that must be later released, otherwise this will be set to NULL
 * @return Success if we have cached data, or an error result if the request was invalid or we do not have cached data.
 *
 * @see EOS_Presence_Info_Release
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Presence_CopyPresence(EOS_HPresence Handle, const EOS_Presence_CopyPresenceOptions* Options, EOS_Presence_Info** OutPresence)
{
	log(LL::Debug, "EOS_Presence_CopyPresence");

	EOS_CHECK_VERSION(EOS_PRESENCE_COPYPRESENCE_API_LATEST);

	PROXY_FUNC(EOS_Presence_CopyPresence);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutPresence);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!Options || !OutPresence)
		return EOS_EResult::EOS_InvalidParameters;

	return EOS_EResult::EOS_AccessDenied;
}

/**
 * Creates a presence modification handle. This handle can used to add multiple changes to your presence that can be applied with EOS_Presence_SetPresence.
 * The resulting handle must be released by calling EOS_PresenceModification_Release once it has been passed to EOS_Presence_SetPresence.
 *
 * @param Options Object containing properties related to the user modifying their presence
 * @param OutPresenceModificationHandle Pointer to a Presence Modification Handle to be set if successful
 * @return Success if we successfully created the Presence Modification Handle pointed at in OutPresenceModificationHandle, or an error result if the input data was invalid
 *
 * @see EOS_PresenceModification_Release
 * @see EOS_Presence_SetPresence
 * @see EOS_PresenceModification_SetStatus
 * @see EOS_PresenceModification_SetRawRichText
 * @see EOS_PresenceModification_SetData
 * @see EOS_PresenceModification_DeleteData
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Presence_CreatePresenceModification(EOS_HPresence Handle, const EOS_Presence_CreatePresenceModificationOptions* Options, EOS_HPresenceModification* OutPresenceModificationHandle)
{
	log(LL::Debug, "EOS_Presence_CreatePresenceModification");

	EOS_CHECK_VERSION(EOS_PRESENCE_CREATEPRESENCEMODIFICATION_API_LATEST);

	PROXY_FUNC(EOS_Presence_CreatePresenceModification);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutPresenceModificationHandle);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!Options || !OutPresenceModificationHandle)
		return EOS_EResult::EOS_InvalidParameters;

	return EOS_EResult::EOS_AccessDenied;
}

/**
 * Sets your new presence with the data applied to a PresenceModificationHandle. The PresenceModificationHandle can be released safely after calling this function.
 *
 * @param Options Object containing a PresenceModificationHandle and associated user data
 * @param ClientData Optional pointer to help track this request, that is returned in the completion callback
 * @param CompletionDelegate Pointer to a function that handles receiving the completion information
 *
 * @see EOS_Presence_CreatePresenceModification
 * @see EOS_PresenceModification_Release
 */
EOS_DECLARE_FUNC(void) EOS_Presence_SetPresence(EOS_HPresence Handle, const EOS_Presence_SetPresenceOptions* Options, void* ClientData, const EOS_Presence_SetPresenceCompleteCallback CompletionDelegate)
{
	log(LL::Debug, "EOS_Presence_SetPresence");

	EOS_CHECK_VERSION(EOS_PRESENCE_SETPRESENCE_API_LATEST);

	PROXY_FUNC(EOS_Presence_SetPresence);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_Presence_SetPresenceOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_Presence_SetPresenceCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;

			CompletionDelegate(&cbi);
		});
}

/**
 * Register to receive notifications when presence changes. If the returned NotificationId is valid, you must call RemoveNotifyOnPresenceChanged when you no longer wish to
 * have your NotificationHandler called
 *
 * @param ClientData Data the is returned to when NotificationHandler is invoked
 * @param NotificationHandler The callback to be fired when a presence change occurs
 * @return Notification ID representing the registered callback if successful, an invalid NotificationId if not
 *
 * @see EOS_ENotificationId::EOS_NotificationId_Invalid
 * @see EOS_Presence_RemoveNotifyOnPresenceChanged
 */
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_Presence_AddNotifyOnPresenceChanged(EOS_HPresence Handle, const EOS_Presence_AddNotifyOnPresenceChangedOptions* Options, void* ClientData, const EOS_Presence_OnPresenceChangedCallback NotificationHandler)
{
	log(LL::Debug, "EOS_Presence_AddNotifyOnPresenceChanged");

	EOS_CHECK_VERSION(EOS_PRESENCE_ADDNOTIFYONPRESENCECHANGED_API_LATEST);

	PROXY_FUNC(EOS_Presence_AddNotifyOnPresenceChanged);
	if (proxied)
	{
		auto res = proxied(Handle, Options, ClientData, NotificationHandler);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	return EOS_INVALID_NOTIFICATIONID;
}

/**
 * Unregister a previously bound notification handler from recieving presence update notifications
 *
 * @param NotificationId The Notification ID representing the registered callback
 */
EOS_DECLARE_FUNC(void) EOS_Presence_RemoveNotifyOnPresenceChanged(EOS_HPresence Handle, EOS_NotificationId NotificationId)
{
	log(LL::Debug, "EOS_Presence_RemoveNotifyOnPresenceChanged");

	PROXY_FUNC(EOS_Presence_RemoveNotifyOnPresenceChanged);
	if (proxied)
	{
		proxied(Handle, NotificationId);
		return;
	}

	return;
}

/**
 * To modify your own presence, you must call EOS_Presence_CreatePresenceModification to create a Presence Modification handle. To modify that handle, call
 * EOS_PresenceModification_* methods. Once you are finished, call EOS_Presence_SetPresence with your handle. You must then release your Presence Modification
 * handle by calling EOS_PresenceModification_Release.
 */

 /**
  * Modifies a user's online status to be the new state.
  *
  * @param Options Object containing properties related to setting a user's Status
  * @return Success if modification was added successfully, otherwise an error code related to the problem
  */
EOS_DECLARE_FUNC(EOS_EResult) EOS_PresenceModification_SetStatus(EOS_HPresenceModification Handle, const EOS_PresenceModification_SetStatusOptions* Options)
{
	log(LL::Debug, "EOS_PresenceModification_SetStatus");

	EOS_CHECK_VERSION(EOS_PRESENCE_SETSTATUS_API_LATEST);

	PROXY_FUNC(EOS_PresenceModification_SetStatus);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	return EOS_EResult::EOS_Success;
}

/**
 * Modifies a user's Rich Presence string to a new state. This is the exact value other users will see
 * when they query the local user's presence.
 *
 * @param Options Object containing properties related to setting a user's RichText string
 * @return Success if modification was added successfully, otherwise an error code related to the problem
 *
 * @see EOS_PRESENCE_RICH_TEXT_MAX_VALUE_LENGTH
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_PresenceModification_SetRawRichText(EOS_HPresenceModification Handle, const EOS_PresenceModification_SetRawRichTextOptions* Options)
{
	log(LL::Debug, "EOS_PresenceModification_SetRawRichText");

	EOS_CHECK_VERSION(EOS_PRESENCE_SETRAWRICHTEXT_API_LATEST);

	PROXY_FUNC(EOS_PresenceModification_SetRawRichText);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	return EOS_EResult::EOS_Success;
}

/**
 * Modifies one or more rows of user-defined presence data for a local user. At least one InfoData object
 * must be specified.
 *
 * @param Options Object containing an array of new presence data.
 * @return Success if modification was added successfully, otherwise an error code related to the problem
 *
 * @see EOS_PRESENCE_DATA_MAX_KEYS
 * @see EOS_PRESENCE_DATA_MAX_KEY_LENGTH
 * @see EOS_PRESENCE_DATA_MAX_VALUE_LENGTH
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_PresenceModification_SetData(EOS_HPresenceModification Handle, const EOS_PresenceModification_SetDataOptions* Options)
{
	log(LL::Debug, "EOS_PresenceModification_SetData");

	EOS_CHECK_VERSION(EOS_PRESENCE_SETDATA_API_LATEST);

	PROXY_FUNC(EOS_PresenceModification_SetData);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	return EOS_EResult::EOS_Success;
}

/**
 * Removes one or more rows of user-defined presence data for a local user. At least one DeleteDataInfo object
 * must be specified.
 *
 * @param Options Object containing an array of new presence data.
 * @return Success if modification was added successfully, otherwise an error code related to the problem
 *
 * @see EOS_PRESENCE_DATA_MAX_KEYS
 * @see EOS_PRESENCE_DATA_MAX_KEY_LENGTH
 * @see EOS_PRESENCE_DATA_MAX_VALUE_LENGTH
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_PresenceModification_DeleteData(EOS_HPresenceModification Handle, const EOS_PresenceModification_DeleteDataOptions* Options)
{
	log(LL::Debug, "EOS_PresenceModification_DeleteData");

	EOS_CHECK_VERSION(EOS_PRESENCE_DELETEDATA_API_LATEST);

	PROXY_FUNC(EOS_PresenceModification_DeleteData);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	return EOS_EResult::EOS_Success;
}

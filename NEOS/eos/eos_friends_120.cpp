#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Listen for changes to friends for a particular account.
 *
 * @param Options Information about who would like notifications.
 * @param ClientData This value is returned to the caller when FriendsUpdateHandler is invoked.
 * @param FriendsUpdateHandler The callback to be invoked when a change to any friend status changes.
 * @return A valid notification ID if successfully bound, or EOS_INVALID_NOTIFICATIONID otherwise
 */
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_Friends_AddNotifyFriendsUpdate(EOS_HFriends Handle, const EOS_Friends_AddNotifyFriendsUpdateOptions* Options, void* ClientData, const EOS_Friends_OnFriendsUpdateCallback FriendsUpdateHandler)
{
	if (Options)
		log(LL::Debug, "EOS_Friends_AddNotifyFriendsUpdate (%p(%d))", Options, Options->ApiVersion);
	else
		log(LL::Debug, "EOS_Friends_AddNotifyFriendsUpdate (%p)", Options);

	EOS_CHECK_VERSION(EOS_FRIENDS_ADDNOTIFYFRIENDSUPDATE_API_LATEST);

	PROXY_FUNC(EOS_Friends_AddNotifyFriendsUpdate);
	if (proxied)
	{
		auto res = proxied(Handle, Options, ClientData, FriendsUpdateHandler);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	return EOS_INVALID_NOTIFICATIONID;
}

/**
 * Stop listening for friends changes on a previously bound handler.
 *
 * @param NotificationId The previously bound notification ID.
 */
EOS_DECLARE_FUNC(void) EOS_Friends_RemoveNotifyFriendsUpdate(EOS_HFriends Handle, EOS_NotificationId NotificationId)
{
	log(LL::Debug, "EOS_Friends_RemoveNotifyFriendsUpdate (%lld)", NotificationId);

	PROXY_FUNC(EOS_Friends_RemoveNotifyFriendsUpdate);
	if (proxied)
	{
		proxied(Handle, NotificationId);
		return;
	}

	return;
}

#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * The Platform Instance is used to gain access to all other Epic Online Service interfaces and to drive internal operations through the Tick.
 * All Platform Instance calls take a handle of type EOS_HPlatform as the first parameter.
 * EOS_HPlatform handles are created by calling EOS_Platform_Create and subsequently released by calling EOS_Platform_Release.
 *
 * @see eos_init.h
 * @see EOS_Initialize
 * @see EOS_Platform_Create
 * @see EOS_Platform_Release
 * @see EOS_Shutdown
 */

/**
 * Notify the platform instance to do work. This function must be called frequently in order for the services provided by the SDK to properly
 * function. For tick-based applications, it is usually desireable to call this once per-tick.
 */
EOS_DECLARE_FUNC(void) EOS_Platform_Tick(EOS_HPlatform Handle)
{
	// Note: no log of this function as it's run VERY often!

	// Run all our callbacks
	NEOS_RunCallbacks();

	if (!Handle)
		return;

	auto* platform = reinterpret_cast<HPlatform*>(Handle);
	PROXY_FUNC(EOS_Platform_Tick);
	if (proxied)
	{
		proxied(platform->GetProxyHandle());
		return;
	}
}

// ugh - can't find a good way to get rid of this warning...
#pragma warning( push )
#pragma warning( disable : 4312 )
/**
 * Get a handle to the Metrics Interface.
 * @return EOS_HMetrics handle
 *
 * @see eos_metrics.h
 * @see eos_metrics_types.h
 */
EOS_DECLARE_FUNC(EOS_HMetrics) EOS_Platform_GetMetricsInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetMetricsInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetMetricsInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_HMetrics>(NEOS_HMETRICS);
}

/**
 * Get a handle to the Auth Interface.
 * @return EOS_HAuth handle
 *
 * @see eos_auth.h
 * @see eos_auth_types.h
 */
EOS_DECLARE_FUNC(EOS_HAuth) EOS_Platform_GetAuthInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetAuthInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetAuthInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_HAuth>(platform->GetHAuth());
}

/**
 * Get a handle to the Ecom Interface.
 * @return EOS_HEcom handle
 *
 * @see eos_ecom.h
 * @see eos_ecom_types.h
 */
EOS_DECLARE_FUNC(EOS_HEcom) EOS_Platform_GetEcomInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetEcomInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetEcomInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		platform->GetHEcom()->SetProxyHandle(res);
	}

	return reinterpret_cast<EOS_HEcom>(platform->GetHEcom());
}

/**
 * Get a handle to the Friends Interface.
 * @return EOS_HFriends handle
 *
 * @see eos_friends.h
 * @see eos_friends_types.h
 */
EOS_DECLARE_FUNC(EOS_HFriends) EOS_Platform_GetFriendsInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetFriendsInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetFriendsInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_HFriends>(NEOS_HFRIENDS);
}

/**
 * Get a handle to the Presence Interface.
 * @return EOS_HPresence handle
 *
 * @see eos_presence.h
 * @see eos_presence_types.h
 */
EOS_DECLARE_FUNC(EOS_HPresence) EOS_Platform_GetPresenceInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetPresenceInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetPresenceInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_HPresence>(NEOS_HPRESENCE);
}

/**
 * Get a handle to the UserInfo Interface.
 * @return EOS_HUserInfo handle
 *
 * @see eos_userinfo.h
 * @see eos_userinfo_types.h
 */
EOS_DECLARE_FUNC(EOS_HUserInfo) EOS_Platform_GetUserInfoInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetUserInfoInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetUserInfoInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_HUserInfo>(NEOS_HUSERINFO);
}
#pragma warning( pop )

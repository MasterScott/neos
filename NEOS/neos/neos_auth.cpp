#include "../stdafx.h"
#include "../NEOS.hpp"

// Thread-safe: adds callback to login callbacks vector
// Returns index of the added callback
size_t HAuth::LoginCallbackAdd(std::function<void()> Func)
{
	std::lock_guard<std::mutex> guard(m_LoginCallbacksMutex);

	m_LoginCallbacks.push_back(Func);
	return m_LoginCallbacks.size() - 1;
}

// Thread-safe: removes a callback from login callbacks vector
void HAuth::LoginCallbackRemove(size_t ID)
{
	std::lock_guard<std::mutex> guard(m_LoginCallbacksMutex);

	if (ID >= m_LoginCallbacks.size())
	{
		log(LL::Warning, __FUNCTION__ ": invalid ID provided (%d), max %d", ID, m_LoginCallbacks.size());
		return;
	}

	m_LoginCallbacks.erase(m_LoginCallbacks.begin() + ID);
}

void HAuth::ChangeLoginStatus(EOS_ELoginStatus NewStatus)
{
	if (NewStatus == m_LoginStatus)
		return; // no change, return..

	{
		std::lock_guard<std::mutex> guard(m_LoginStatusMutex);
		m_PrevStatus = m_LoginStatus;
		m_LoginStatus = NewStatus;

		log(LL::Debug, __FUNCTION__ ": %d -> %d", m_PrevStatus, m_LoginStatus);
	}

	{
		std::lock_guard<std::mutex> guard(m_LoginCallbacksMutex);
		for (auto callback : m_LoginCallbacks)
			callback();
	}
}

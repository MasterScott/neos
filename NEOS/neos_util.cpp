#include "stdafx.h"

LPCWSTR GetCommandLineNoExe()
{
	// From wine shell32_main.c CommandLineToArgvW
	LPCWSTR s = GetCommandLineW();
	/* The first argument, the executable path, follows special rules */
	if (*s == '"')
	{
		/* The executable path ends at the next quote, no matter what */
		s++;
		while (*s)
			if (*s++ == '"')
				break;
	}
	else
	{
		/* The executable path ends at the next space, no matter what */
		while (*s && *s != ' ' && *s != '\t')
			s++;
	}
	/* skip to the first argument, if any */
	while (*s == ' ' || *s == '\t')
		s++;

	return s;
}

std::filesystem::path GetDllParentDir(HMODULE Module)
{
	WCHAR ModulePathStr[4096];
	GetModuleFileNameW(Module, ModulePathStr, 4096);

	std::filesystem::path ModulePath = ModulePathStr;
	return ModulePath.parent_path();
}

std::wstring WidenString(const std::string& Input)
{
	if (Input.empty())
		return L"";
	// Calculate target buffer size (not including the zero terminator).
	int len = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS,
		Input.c_str(), (int)Input.size(), NULL, 0);
	if (len == 0)
		return L"";

	std::wstring output(len, 0);
	// No error checking. We already know, that the conversion will succeed.
	MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS,
		Input.c_str(), (int)Input.size(), output.data(), (int)output.size());

	return output;
}

std::string ToString(uint64_t Input)
{
	std::ostringstream os;
	os << Input;
	return os.str();
}

bool StringToBool(const std::string& Input)
{
	std::string lower_str = Input;
	std::transform(lower_str.begin(), lower_str.end(), lower_str.begin(), ::tolower);

	return lower_str == "1" || lower_str == "true" || lower_str == "yes";
}

bool FileExists(const char* Path)
{
	return GetFileAttributesA(Path) != -1;
}
